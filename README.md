
# Chainlabs Excercise
- [app](app/) folder contains app source code and helm chart.
- [ci/templates/argo-app](ci/templates/argo-app) contains a helm chart used for templating preview environments.
- [environments/development/argo-cd-manifests](environments/development/argo-cd-manifests) contains everything that is managed by [argocd](https://argocd.namikmesic.com).
- [environments/development/kubernetes-manifests](environments/development/kubernetes-manifests) is self explanatory. This contains everything that is a kubernetes api spec.
- [environments/development/terraform](environments/development/terraform)  contains all of the Terraform manifests.

# Dependent repos
- Preview-apps repository: https://gitlab.com/namikmesic/chainlabs-preview

# Disclaimer
I am treating this as POC, not a production environment. I made a lot of shortcuts that are not acceptable in production.
I'd be happy to discuss improvements. There can also be some optimization on pipeline workflows, and pipeline triggering to avoid unnecessary time wasting. For example, subsequent docker image builds are unnecessary if there are no changes in-app or Docker image.
 
Git history will be terrible, but I assure you I can be neat :). I rushed trought his, and many of the tools I used in this excercise are new to me as well.


# Terraform
We are using simple Terraform modules to provision VPC + EKS cluster.

Disclaimer: I did try to use terragrunt, as I liked the idea of "Dry" configurations, but experienced some issues with eks module, due to the fact that it does require terraform resources for managing config-map.
In order to save time, I opted for a normal Terraform manifests.
Terraform is using gitlab http backend, and their [proposed workflow](https://docs.gitlab.com/ee/user/infrastructure/iac/).


# Development Workflow 

### Checking out and pushing branch

Examples:
 
- Url: https://gitlab.com/namikmesic/chainlabs-excercise/-/tree/somefeature1
- Initial branch push pipeline: https://gitlab.com/namikmesic/chainlabs-excercise/-/pipelines/417848533
 
- Initial branch push pipeline: https://gitlab.com/namikmesic/chainlabs-excercise/-/pipelines/417860147
 
Note that gitlab rules with "[changes](https://gitlab.com/namikmesic/chainlabs-excercise/-/blob/main/.gitlab-ci.yml#L33)" rules will not work on initial branch push.
This is due to gitlab limitation to track changes in files from a brand new branch.
It does not have state to compare it to, and checking against main branch is still not a supported feature.
 
Each consequent push will trigger only pipelines for app, unless you have terraform changes as well in your commits. This should in theory will speed up the build process in the long run.
 
Disclaimer: I feel that having separate repositories for infra / app would be easier to work with Gitlab as it is very opinionated.
For example, environments and [Terraform reports](https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html) do not seem to work well together, so you won't be able to see a Terraform report in the PR link.
This would not be the case if we only run Terraform pipelines without using the environments feature. Since I do not have a lot of time I will just acknowledge this as a problem.


### PR

- PR: https://gitlab.com/namikmesic/chainlabs-excercise/-/merge_requests/31
- Pipeline will present us with a preview environment: https://somefeature.namikmesic.com
 
The way everything is deployed on this cluster is through the use of argo-cd applications. The "[app_preview](https://gitlab.com/namikmesic/chainlabs-excercise/-/blob/main/.gitlab-ci.yml#L119)" pipeline, just generates argocd application template and pushes it
to the [preview repository](https://gitlab.com/namikmesic/chainlabs-preview/-/tree/main/preview-environments) that is defined in the [app of apps](https://gitlab.com/namikmesic/chainlabs-excercise/-/blob/main/environments/development/argo-cd-manifests/argo-cd-apps/preview-apps.yaml) for preview environments. Any time there is a new argocd crd manifest added to the repo, argocd will deploy it. It will also prune the application if you remove the app manifest,
which is what happens in the "[stop_review_app](https://gitlab.com/namikmesic/chainlabs-excercise/-/blob/main/.gitlab-ci.yml#L164)" pipeline.
 
It is also important to note, that environments have 1 day to live as the [autostop](https://gitlab.com/namikmesic/chainlabs-excercise/-/blob/main/.gitlab-ci.yml#L127) feature is enabled. In case PR is alive more than 1 day, you would need to re-trigger "app-preview" stage.


### Merge
On merge, nothing will happen unless we update the main app chart definition, + making a tag. When we make a new tag we will run both terraform apply and app_release stages. 

### Release
Example: 

- Pipelines on merge: https://gitlab.com/namikmesic/chainlabs-excercise/-/pipelines/417864792
- Pipelines on tag: https://gitlab.com/namikmesic/chainlabs-excercise/-/pipelines/417867055
- Finally adjusting the chart image version to rollout new app version: https://gitlab.com/namikmesic/chainlabs-excercise/-/merge_requests/33

Argocd will pick up the change and will create a new argo-rollout. If everything works, we should see our changes live at https://staging.namikmesic.com
Release pipelines example: https://gitlab.com/namikmesic/chainlabs-excercise/-/pipelines/417867055
 
 
If there is an unforeseen problem with i.e configuration or service, then we should not experience any downtime at all, but the rollout will fail.
We can also demonstrate this live. Ideally, we should have alerts on rollout metrics with different channels (page, slack, etc.).


# Bootstraping cluster, staging environment and everything else
Everything that is deployed in the eks cluster is managed by argo-cd, including argo-cd itself. It is through a combination of different helm-charts and kustomize definitions.
 
Perhaps you have noticed, a staging environment is defined in environments/development/argo-cd-manifest/argo-cd-apps/. We have a different application for redis, and a different one for the app itself. 

Chart [values](https://gitlab.com/namikmesic/chainlabs-excercise/-/blob/main/environments/development/argo-cd-manifests/argo-cd-apps/staging-app.yaml#L16) for staging environment are slightly different as it is using Redis cluster and Argo-rollouts.

Staging environment is using blue/green [argo rollout](https://gitlab.com/namikmesic/chainlabs-excercise/-/blob/main/app/chart/app/templates/deploy.yaml#L1) with blue/green strategy.

[Normal development environment](https://gitlab.com/namikmesic/chainlabs-excercise/-/blob/main/ci/templates/argo-app/templates/preview.yaml#L20) are deployed using normal kubernetes deployments, and standalone single redis master replica.

I did set-up ingress-nginx with proper metrics for canary releases, but I did not have time to make this happen, however we could introduce [Argo-rollouts prometheus analysis](https://argoproj.github.io/argo-rollouts/analysis/prometheus/) and a [canary strategy](https://argoproj.github.io/argo-rollouts/features/canary/).
 
For stitching everything together we are using a combination of tools:
- argo-cd
- argo-rollouts
- ingress-nginx
- sealed-secrets
- bitnami redis helm-chart
 
Argocd url: https://argocd.namikmesic.com

