data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"

  cluster_version = "1.21"
  cluster_name    = "${var.environment}-cluster"
  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.private_subnets
  write_kubeconfig = false
  
  worker_groups = [
    {
      instance_type = var.eks_worker_instace_family
      asg_max_size  = var.eks_worker_max_instance_num
      asg_desired_capacity = 3
    }
  ]
  cluster_tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}
