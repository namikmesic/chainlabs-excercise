variable "aws_region" {
  type = string
}

variable "aws_role_arn" {
  type = string
}

variable "environment" {
  type = string
}

variable "eks_worker_instace_family" {
  type = string
}
variable "eks_worker_max_instance_num" {
  type = number
}

variable "vpc_cidr" {
  type = string
}

variable "vpc_private_cidr" {
  type = list(string)
}
variable "vpc_public_cidr" {
  type = list(string)
}