aws_region   = "eu-west-1"
aws_role_arn = "arn:aws:iam::262481817154:role/OrganizationAccountAccessRole"
environment  = "development"

vpc_cidr         = "10.0.0.0/16"
vpc_private_cidr = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
vpc_public_cidr  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

eks_worker_instace_family   = "t2.medium"
eks_worker_max_instance_num = 5